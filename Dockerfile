FROM node

RUN apt-get update 
# RUN apt-get upgrade -y
RUN apt-get install sudo curl git tmux -y
RUN apt-get install locales-all -y

# RUN useradd -ms /bin/bash dev
# USER dev

RUN export LC_CTYPE=em_US.UTF-8
RUN git clone https://github.com/c9/core.git /c9sdk
RUN ( cd /c9sdk && scripts/install-sdk.sh )

RUN mkdir /workspace
COPY onentry.sh /

ENV PORT 9090
ENV HOST 0.0.0.0
ENV LOGIN_USERNAME ''
ENV LOGIN_PASSWORD ''
ENV COLLAB false
ENV WORKSPACE "/workspace"

ENTRYPOINT ["/bin/bash","/onentry.sh"]
