#!/bin/bash
node /c9sdk/server.js \
--listen=$HOST --port=$PORT \
--auth=$LOGIN_USERNAME:$LOGIN_PASSWORD \
--collab $COLLAB \
-w $WORKSPACE
